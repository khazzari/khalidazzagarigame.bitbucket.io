class Game {
    constructor() {
        this._element = document.getElementById('container');
        this.keyboardHandler = (e) => {
            if (e.keyCode === 32) {
                this._player.move(50);
            }
            else if (e.keyCode === 37) {
                this._player2.move(50);
            }
            this.update();
        };
        this._player = new Player('car', 1300, 340);
        this._player2 = new Player('car2', 1300, 100);
        this._parkinglot = new ParkingLot('parkPlace', 0, 320);
        this._parkinglot2 = new ParkingLot('parkingLot', 0, 300);
        this._parkinglot3 = new ParkingLot('parkPlace2', 0, 320);
        this._parkinglot4 = new ParkingLot('parkingLot2', 0, 300);
        this._score = new Score("Score");
        window.addEventListener("keydown", this.keyboardHandler);
        this.draw();
    }
    draw() {
        this._parkinglot.draw(this._element);
        this._parkinglot2.draw(this._element);
        this._parkinglot3.draw(this._element);
        this._parkinglot4.draw(this._element);
        this._player.draw(this._element);
        this._player2.draw(this._element);
        this._score.draw(this._element);
    }
    /**
     * Function to update the state of all living objects
     */
    update() {
        this.collision();
        this._player.update();
        this._player2.update();
        this._score.update();
    }
    collision() {
        //use elem.getBoundingClientRect() for getting the wright coordinates and measurements of the element
        const parkPlaceRect = document.getElementById('parkPlace').getBoundingClientRect();
        const carRect = document.getElementById('car').getBoundingClientRect();
        if (parkPlaceRect.left <= carRect.left && parkPlaceRect.right >= carRect.right) {
            window.removeEventListener('keydown', this.keyboardHandler);
            this._score.addScore();
            this._player.resetPosition(1300, 340);
            window.addEventListener('keydown', this.keyboardHandler);
        }
        else {
            console.log('no collision');
        }
    }
}
class GameItems {
    // constructor of the GameItems.
    constructor(name, xPosition = 0, yPosition = 0) {
        this._name = name;
        this._xPos = xPosition;
        this._yPos = yPosition;
    }
    set xPos(xPosition) {
        this._xPos = xPosition;
    }
    set yPos(yPosition) {
        this._yPos = yPosition;
    }
    draw(container) {
        //create div
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        //create image
        const image = document.createElement('img');
        image.src = `./images/${this._name}.png`;
        image.style.width = "100%";
        //append elements
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
}
//this is my constant where game and events meet
let app;
(function () {
    /**
     * Run after dom is ready
     */
    let init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
class ParkingLot extends GameItems {
    constructor(name, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
    }
}
class Player extends GameItems {
    constructor(name, xPosition, yPosition) {
        super(name, xPosition, yPosition);
    }
    move(xPosition) {
        this._xPos -= xPosition;
        this._element.classList.add('moving');
    }
    resetPosition(xPos, yPos) {
        this._xPos = 1300;
        this._yPos = 320;
    }
}
class Score extends GameItems {
    constructor(name) {
        super(name);
        this._score = 0;
    }
    get score() {
        return this._score;
    }
    /**
    * Function to draw the initial state of the gameItem
    * @param {HTMLElement} - container
    */
    draw(container) {
        //create div
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        //create p
        const p = document.createElement('p');
        p.innerHTML = 'The score is: ';
        //create span
        const span = document.createElement('span');
        span.innerHTML = this._score.toString();
        //append elements
        p.appendChild(span);
        this._element.appendChild(p);
        container.appendChild(this._element);
    }
    /**
    * Function to update the state of the Scoreboard in the DOM
    */
    update() {
        //get the contents of span
        const scoreSpan = this._element.childNodes[0].childNodes[1];
        scoreSpan.innerHTML = this._score.toString();
    }
    /**
    * Function to add the score with 1
    */
    addScore() {
        this._score += 1;
    }
}
