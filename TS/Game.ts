class Game {

    private _element: HTMLElement = document.getElementById('container');
    private _player: Player;
    private _player2: Player;
    private _score: Score;
    private _gameItems: GameItems;
    private _parkinglot: ParkingLot;
    private _parkinglot2: ParkingLot;
    private _parkinglot3: ParkingLot;
    private _parkinglot4: ParkingLot;

    constructor() {
    this._player = new Player('car', 1300, 340);
    this._player2 = new Player('car2', 1300, 100);

     this._parkinglot = new ParkingLot('parkPlace', 0, 320);
     this._parkinglot2 = new ParkingLot('parkingLot', 0, 300);

     this._parkinglot3 = new ParkingLot('parkPlace2', 0, 320);
     this._parkinglot4 = new ParkingLot('parkingLot2', 0, 300);
      this._score = new Score("Score");

        window.addEventListener("keydown", this.keyboardHandler);
        
        this.draw();


    }
    public draw(): void {
        this._parkinglot.draw(this._element);
        this._parkinglot2.draw(this._element);

        this._parkinglot3.draw(this._element);
        this._parkinglot4.draw(this._element);

        this._player.draw(this._element);
        this._player2.draw(this._element);

        this._score.draw(this._element);

      }
    
      /**
       * Function to update the state of all living objects
       */
      public update(): void { //function formerly known as render()
        this.collision();
        this._player.update();
        this._player2.update();
        this._score.update();
      }

      public keyboardHandler = (e: KeyboardEvent): void => {
        if(e.keyCode === 32) {
            this._player.move(50);
        }
        else if(e.keyCode === 37) {
            this._player2.move(50);
        }
        this.update();
    }


    public collision(): void {
        //use elem.getBoundingClientRect() for getting the wright coordinates and measurements of the element
        const parkPlaceRect = document.getElementById('parkPlace').getBoundingClientRect();
        const carRect = document.getElementById('car').getBoundingClientRect();

        if (parkPlaceRect.left <= carRect.left && parkPlaceRect.right >= carRect.right) {
            window.removeEventListener('keydown', this.keyboardHandler);
            this._score.addScore();
            this._player.resetPosition(1300, 340);
            window.addEventListener('keydown', this.keyboardHandler);
        } else {
          console.log('no collision');
        }
      }

}