class Player extends GameItems {

    constructor(name: string, xPosition: number, yPosition: number){
        super(name, xPosition, yPosition);
    }

    public move(xPosition: number): void {
        this._xPos -= xPosition;
        this._element.classList.add('moving');
    }

    public resetPosition(xPos: number, yPos: number): void {
        this._xPos = 1300;
        this._yPos = 320;
    }
}